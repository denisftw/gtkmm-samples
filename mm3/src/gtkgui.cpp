#include <gtkmm.h>

class MyWindow : public Gtk::Window {
public:
  MyWindow() {
    auto label = Gtk::make_managed<Gtk::Label>("Hello World from GTKmm3!");
    this->add(*label);
    set_title("My application");
    set_default_size(250, 80);
    label->show();
  }
};

int main() {
  auto app = Gtk::Application::create("cpp.gtksample.HelloWorld");
  MyWindow win;
  return app->run(win, 0, nullptr);
}
