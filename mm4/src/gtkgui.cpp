#include <gtkmm.h>

class MyWindow : public Gtk::Window {
public:
  MyWindow() {
    auto label = Gtk::make_managed<Gtk::Label>("Hello World from GTKmm4!");
    this->set_child(*label);
    set_title("My application");
    set_default_size(250, 80);
  }
};

int main() {
  auto app = Gtk::Application::create("cpp.gtksample.HelloWorld");
  return app->make_window_and_run<MyWindow>(0, nullptr);
}
